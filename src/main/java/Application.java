import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        OwnVector vector1 = new OwnVector(3,4,5);
        OwnVector vector2 = new OwnVector(2,3,4);

        System.out.println("First vector: " + vector1);
        System.out.println("Second vector: " + vector2);
        System.out.println();

        System.out.println("Vector1 length: " + vector1.length());
        System.out.println("Vector2 length: " + vector2.length());

        System.out.println("Scalar multiplication of vectors: " + vector1.scalarMultiplication(vector2));
        System.out.println("Vector multiplication: " + vector1.vectorMultiplicationNew(vector2).toString());
        System.out.println("Cos Angle of vectors: " + vector1.angle(vector2));

        System.out.println("Sum of vectors: " + vector1.sum(vector2).toString()); //явный вызов toString()
        System.out.println("Subtraction of vectors: " + vector1.subtraction(vector2)); //неявный вызов toString()

        System.out.println();
        System.out.println("Enter the number of vectors");
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        scanner.close();
        int counter = 1;
        for (OwnVector item: OwnVector.randomVectors(N)) {
            System.out.println(counter + " vector: " + item);
            counter++;
        }
    }
}
