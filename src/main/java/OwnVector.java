import java.util.ArrayList;
import java.util.List;
import java.util.Random;

final public class OwnVector {
    private final int x;
    private final int y;
    private final int z;

    public OwnVector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return  "{" + x +
                ";" + y +
                ";" + z +
                '}';
    }

    public double length(){ //|a| = sqrt(x^2 + y^2 + z^2)
        return Math.round(Math.sqrt(x*x + y*y + z*z));
    }

    public double length(OwnVector v){
        return Math.round(Math.sqrt(v.x*v.x + v.y*v.y + v.z*v.z));
    }

    public int scalarMultiplication(OwnVector v){ // u * v = x1*x2 + y1*y2 + z1*z2
        return x * v.x + y * v.y + z * v.z;
    }

    public OwnVector vectorMultiplicationNew(OwnVector v){
        return new OwnVector(y*v.z - v.y*z, -(x*v.z - v.x*z), x*v.y - v.x*y);
    }

    public double angle(OwnVector v){ //(x1*x2 + y1*y2 + z1*z2) / sqrt(x1^2 + y1^2 + z1^2) + sqrt(x2^2 + y2^2 + z2^2)
        return (scalarMultiplication(v)) / (length() * length(v));
    }

    public OwnVector sum(OwnVector v){
        return new OwnVector(x + v.x, y + v.y, z + v.z);
    }

    public OwnVector subtraction(OwnVector v){
        return new OwnVector(x - v.x, y - v.y, z - v.z);
    }

    public static List<OwnVector> randomVectors(int N){
        List<OwnVector> vectors = new ArrayList<>(N);
        final Random random = new Random();
        for (int i = 0; i < N; i++) {
            int x = random.nextInt(10);
            int y = random.nextInt(10);
            int z = random.nextInt(10);
            OwnVector vector = new OwnVector(x,y,z);
            vectors.add(vector);
        }
        return vectors;
    }
}
